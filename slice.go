package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

/*
Write a program which prompts the user to enter integers and stores the integers in a sorted slice.
The program should be written as a loop. Before entering the loop, the program should create an
empty integer slice of size (length) 3. During each pass through the loop, the program prompts
the user to enter an integer to be added to the slice. The program adds the integer to the slice,
sorts the slice, and prints the contents of the slice in sorted order. The slice must grow in size
to accommodate any number of integers which the user decides to enter.
The program should only quit (exiting the loop) when the user enters the character ‘X’ instead of an integer.
 */


func main() {

	//Assuming we are meant to replace the initial zero values
	sli := make([]int, 0, 3)
	fmt.Println("Enter integers, or enter X to quit")
	for {
		var input string
		fmt.Scan(&input)

		//Accepting x or X to quit
		if strings.ToUpper(input) == "X"{
			break
		}

		inputAsInt, err := strconv.Atoi(input)
		//If value was not parsable as an int, have user try again as our only exit condition is the value X
		if err != nil {
			fmt.Println("Invalid input. Try again")
			continue
		}

		sli = append(sli, inputAsInt)

		sort.Ints(sli)
		fmt.Println(sli)

	}

}