package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main(){

	/*
		Write a program which prompts the user to enter a string.
		The program searches through the entered string for the characters ‘i’, ‘a’, and ‘n’.
		The program should print “Found!” if the entered string starts with the character ‘i’,
		ends with the character ‘n’, and contains the character ‘a’.
		The program should print “Not Found!” otherwise. The program should not be case-sensitive,
		so it does not matter if the characters are upper-case or lower-case.
	*/



	//Scan did not like spaces, so using ReadString instead
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter a string: ")
	userinput, _ := reader.ReadString('\n')

	//Trim newline and convert to lower case so we can compare easily
	var userinputL = strings.ToLower(strings.TrimSpace(userinput))

	prefix := "i"
	suffix := "n"
	exists := "a"

	containsChar := strings.Contains(userinputL, exists)
	correctPrefix := strings.HasPrefix(userinputL, prefix)
	correctPostfix := strings.HasSuffix(userinputL, suffix)

	if correctPrefix && correctPostfix && containsChar {
		fmt.Println("Found!")
	}else{
		fmt.Println("Not Found!")
	}





}