package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"unicode/utf8"
)

/*
Write a program which reads information from a file and represents it in a slice of structs.
Assume that there is a text file which contains a series of names.
Each line of the text file has a first name and a last name, in that order, separated by a single space on the line.

Your program will define a name struct which has two fields, fname for the first name, and lname for the last name.
Each field will be a string of size 20 (characters).

Your program should prompt the user for the name of the text file.
Your program will successively read each line of the text file and create a struct which contains
the first and last names found in the file. Each struct created will be added to a slice,
and after all lines have been read from the file, your program will have a slice containing one struct
for each line in the file. After reading all lines from the file, your program should iterate through
your slice of structs and print the first and last names found in each struct.
 */


type Person struct {

	fname string
	lname string

}

func NewPerson(fname string, lname string) Person {
	//Constructor to meet requirement that we have max 20 chars per string.
	//Instructions for this requirement were somewhat vague.
	maxLen := 20
	return Person{fname: MaxLenStr(fname, maxLen), lname: MaxLenStr(lname, maxLen)}
}

func MaxLenStr(s string, maxLen int) string{
	//allow only maxLen runes/characters
	if utf8.RuneCountInString(s) > maxLen {
		temp := []rune(s)
		return string(temp[:maxLen]) //return shortened version
	}else{
		return s //was within maxLen, return as is
	}
}


func main(){

	//Get filename
	fmt.Print("Enter filename: ")
	var filename string
	fmt.Scan(&filename)
	file, err := os.Open(filename)

	defer file.Close() //close eventually

	if err != nil {
		fmt.Println("Couldn't read file")
		os.Exit(1)
	}

	//Collect lines from file, store as structs in slice
	people := make([]Person, 0, 3)
	scanner := bufio.NewScanner(file) //Assuming only first and last are given, no middle, suffix, etc
	for scanner.Scan() {
		fullName := scanner.Text()
		words := strings.Fields(strings.TrimSpace(fullName))
		people = append(people, NewPerson(words[0], words[1]))

	}

	//Print out the first and last for each Person
	for _, p := range people {
		fmt.Printf("First: %s, Last: %s\n", p.fname, p.lname)
	}

}


/*
names.txt contents tested with:
Nick Fury
Charles Xavier
Homer Simpson
Peter Griffin
Tom Bombadil
acdefghijklmnopqrstuvwxyz acdefghijklmnopqrstuvwxyz
 */