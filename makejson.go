package main

import (
	"bufio"
	json "encoding/json"
	"fmt"
	"os"
	"strings"

)

/*
Write a program which prompts the user to first enter a name, and then enter an address.
Your program should create a map and add the name and address to the map using
the keys “name” and “address”, respectively. Your program should use Marshal() to create a JSON object
from the map, and then your program should print the JSON object.
 */



func main(){

	reader := bufio.NewReader(os.Stdin)

	//Get name
	fmt.Println("Enter a name: ")
	inputName, _ := reader.ReadString('\n')
	inputName = strings.TrimSpace(inputName)

	//Get address
	fmt.Println("Enter an address: ")
	inputAddress, _ := reader.ReadString('\n')
	inputAddress = strings.TrimSpace(inputAddress)

	//Map, convert to json, then print
	userMap := map[string]string { "name": inputName, "address": inputAddress }
	marshalledUserMap, _ := json.Marshal(userMap)
	fmt.Println(string(marshalledUserMap))

}

