package main

import (
	"fmt"
	"os"
)

func main(){
	var userinput float64
	fmt.Println("Input a floating point number")
	num, err := fmt.Scan(&userinput)

	if num != 1 || err != nil {
		fmt.Println("Invalid input. Enter a single floating point number")
		os.Exit(1)
	}


	fmt.Println(int(userinput))

}


