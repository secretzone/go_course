package main


/*
ASSIGNMENT:
The goal of this assignment is to create a routine that sorts a series of ten integers using the bubble sort algorithm. Click the "My Submission" tab to view specific instructions for this activity.

Review criteria
less
This assignment is worth a total of 10 points.

Test the program by running it twice and
testing it with a different sequence of integers each time. The first test
sequence of integers should be all positive numbers and the second test should
have at least one negative number. Give 3 points if the program works correctly
for one test sequence, and give 3 more points if the program works correctly
for the second test sequence.

Examine the code. If the code contains a function
called BubbleSort() which takes a slice of integers as an argument, then
give another 2 points.If the code
contains a function called Swap() function which takes two arguments, a slice of
integers and an index value i, then give another 2 points.
 */


import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func getUserInput(sli []int) []int {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter a list of integers and hit return: ")
	userinput, _ := reader.ReadString('\n')
	//fmt.Println("Input as is:")
	//fmt.Println(userinput)


	userinput = strings.TrimSpace(userinput)
	//fmt.Println("Input after trim is:")
	//fmt.Println(userinput)


	inputItems := strings.Fields(userinput)
	//fmt.Println("Input items as a list:")
	//fmt.Println(inputItems)

	for i := 0; i < len(inputItems); i++ {
		num, _ := strconv.Atoi(inputItems[i])
		sli = append(sli, num)
	}

	//fmt.Println("sli after input conversion")
	//fmt.Println(sli)
	return sli
}


func main(){
	userData := make([]int, 0, 10)

	//passing userData NOT as pointer because slice, but perhaps I should have?

	userData = getUserInput(userData)
	userData = bubbleSort(userData)

	fmt.Println("post bubbleSort userData")
	fmt.Println(userData)
}

/*
```
If the code
contains a function called Swap() function which takes two arguments, a slice of
integers and an index value i, then give another 2 points.
```

^^^
Since I don't have access to both items to be swapped,
I must assume swap with next, but this could have been made clearer
*/

func swapPointer(x, y *int) {
	//actually swap
	*x, *y = *y, *x
}

func swap(sli []int, i int){
	//swap with next? not getting both sides as params, so must be
	swapPointer(&sli[i], &sli[i+1])
}

func bubbleSort(sli []int) []int {
	n := len(sli)
	sorted := false

	for !sorted {
		swapped := false
		for i := 0; i < n-1; i++ {
			if sli[i] > sli[i+1] {
				//swapPointer(&sli[i], &sli[i+1])
				swap(sli,i)
				swapped = true
			}
		}
		if !swapped {
			sorted = true
		}
		n = n - 1
	}
	return sli
}